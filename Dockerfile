FROM node:19
WORKDIR /app
COPY package*.json ./
RUN apt update && apt install -y cmake libprotobuf-dev protobuf-compiler
RUN npm ci
COPY . .
RUN npm run build
ENV NODE_ENV=production
# UDP range
ARG UDP_PORT_RANGE_MIN
ENV UDP_PORT_RANGE_MIN=$UDP_PORT_RANGE_MIN
ARG UDP_PORT_RANGE_MAX
ENV UDP_PORT_RANGE_MAX=$UDP_PORT_RANGE_MAX
CMD [ "node", "public/server.js" ]