import {IntermissionGameState} from '../entity/IntermissionGameState.js';
import {GameState} from '../entity/GameState.js';
import {PlayerDto} from '../dto/PlayerDto.js';
import {GeckosServer} from "@geckos.io/server";
import {PlayerAccusationDto} from '../dto/PlayerAccusationDto.js';
import {PlayerSpeakDto} from '../dto/PlayerSpeakDto.js';

export class GameServer {
    private static readonly tickRate: number = 1 // how many times per seconds the game should send updates to players
    private io: GeckosServer
    private state: GameState = new IntermissionGameState()

    constructor(io: GeckosServer) {
        this.io = io
        io.onConnection(channel => {
            channel.on('joinGame', (joiningPlayerDto) => {
                const joiningPlayer = joiningPlayerDto as PlayerDto
                const channelIdIsDefined = channel.id !== undefined
                const channelIdIsTheSameAsSentClientId = channel.id === joiningPlayer.id
                if (channelIdIsDefined && channelIdIsTheSameAsSentClientId) {
                    this.state = this.state.dispatch({name: 'joinGame', value: joiningPlayer})
                }
            })

            channel.on('speak', (speech) => {
                if (channel.id !== undefined) {
                    this.state = this.state.dispatch({
                        name: 'speak',
                        value: {speech, playerId: channel.id} as PlayerSpeakDto
                    })
                }
            })

            channel.on('accusePlayer', (accusedPlayerId) => {
                if (channel.id !== undefined) {
                    this.state = this.state.dispatch({
                        name: 'accusePlayer',
                        value: {accusedPlayerId, accusingPlayerId: channel.id} as PlayerAccusationDto
                    })
                }
            })
            channel.onDisconnect(() => {
                if (channel.id !== undefined) {
                    this.state = this.state.dispatch({name: 'leaveGame', value: {playerId: channel.id}})
                }
            })
        })

        this.gameStatusUpdate()
    }

    private getMillisecondsBetweenFrames(): number {
        return 1000 / GameServer.tickRate;
    }

    gameStatusUpdate = () => {
        const startTime = new Date().getTime();

        this.state = this.state.dispatch({name: 'tick', value: undefined})
        this.io.emit('status', this.state.toDto());

        const runtime = new Date().getTime() - startTime;
        const nextUpdate = Math.max(0, this.getMillisecondsBetweenFrames() - runtime)
        setTimeout(this.gameStatusUpdate, nextUpdate);
    }
}