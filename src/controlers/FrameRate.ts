export class FrameRate {
    private frameRate: number;
    private lastTimestamp: number;

    /**
     * @param smoothingFactor 1 = no smoothing at all (last tick only), 0.5 ~2 ticks,  0 = infinite smoothing
     */
    constructor(private smoothingFactor: number = 0.3) {
        this.frameRate = 0
        this.lastTimestamp = Date.now()
    }

    public tick(): void {
        const currentTimestamp = Date.now()
        const currentTickFramerate = 1000 / Math.max(currentTimestamp - this.lastTimestamp, 1)//avoid infinity in case of 0

        this.frameRate = currentTickFramerate * this.smoothingFactor + this.frameRate * (1 - this.smoothingFactor)

        this.lastTimestamp = currentTimestamp
    }

    public getFramerate(): number {
        return this.frameRate
    }
}