import { AIPlayer } from "../entity/AIPlayer";
import { Player } from "../entity/Player";

export class PlayerQueue {
  private storage: Player[] = [];

  constructor(private capacity: number = Infinity) { }

  fetch(playerId: string): Player {
    const playerFound = this.storage.find(
      (player) => {
        return player.id === playerId
      },
      playerId
    )

    if (playerFound === undefined) throw new Error(`Player ${playerId} not found`)

    return playerFound
  }

  public updatePlayers(mutationCallback: (player: Player) => void) {
    this.storage.forEach(mutationCallback)
  }

  toArray(): Player[] {
    return this.storage;
  }

  public hasPlayer(playerId: string) {
    return this.storage.find(player => player.id === playerId) !== undefined
  }

  public removePlayer(playerId: string): void {
    this.storage = this.storage.filter(({ id }) => playerId !== id);
  }

  public extractPlayer(playerId: string): Player {
    const extractedPlayer = this.fetch(playerId)
    this.removePlayer(playerId)
    return extractedPlayer
  }

  public getOtherPlayers(playerId: string): Player[] {
    return this.storage.filter(({ id }) => playerId !== id);
  }

  static fromArray(players: Player[]): PlayerQueue {
    const playerQueue = new PlayerQueue()
    playerQueue.storage = players
    return playerQueue
  }

  shuffle(): void {
    //from: https://dev.to/codebubb/how-to-shuffle-an-array-in-javascript-2ikj
    for (let i = this.storage.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = this.storage[i];
      this.storage[i] = this.storage[j];
      this.storage[j] = temp;
    }
  }
  peek(): Player {
    if (this.storage.length < 1) {
      throw new Error("Aucun joueur dans la queue")
    }

    return this.storage[0];
  }

  getLastHumans(){
    return this.storage.filter(({ isAI }) => !isAI) as AIPlayer[]
  }

  goToNextPlayer() {
    this.enqueue(this.dequeue());
  }

  getAllAIs():AIPlayer[]{
    return this.storage.filter(({ isAI }) => isAI) as AIPlayer[]
  }

  purgeAIs() {
    this.storage = this.storage.filter(player => {
      return !player.isAI
    });
  }

  enqueue(item: Player): void {
    if (this.size() === this.capacity) {
      throw Error("Queue has reached max capacity, you cannot add more players");
    }
    this.storage.push(item);
  }

  enqueuePlayersFrom(playerQueueToExtractPlayersFrom: PlayerQueue): void {
    const numberOfPlayers = playerQueueToExtractPlayersFrom.size()
    for (let playersLeft = numberOfPlayers; playersLeft > 0; playersLeft--) {
      this.enqueue(playerQueueToExtractPlayersFrom.dequeue())
    }
  }

  isOneHumanLeftOrLess():boolean{
    return this.storage.filter(player => {
      return !player.isAI
    }).length <= 1
  }

  dequeue(): Player {
    if (this.storage.length < 1) {
      throw new Error("Aucun joueur à dépiler")
    }

    return this.storage.shift() as Player;
  }
  size(): number {
    return this.storage.length;
  }
}