import { AnsweringQuestionGameState } from './AnsweringQuestionGameState.js';
import { Action } from '../dto/Action.js';
import { GameState } from "./GameState.js"
import { PlayerDto } from '../dto/PlayerDto.js';
import { Player } from './Player.js';
import { AIPlayer } from './AIPlayer.js';

const ROUND_MAX_TIME = 45
const MIN_AI_ASKING_TIME = 5
const MAX_AI_ASKING_TIME = 30

export class AskingQuestionGameState extends GameState {


    public constructor(previousGameState: GameState | void) {
        if (previousGameState === undefined) {
            super()
            return this
        }
        super(previousGameState)
        // reset all player speeches
        this.inGamePlayers.updatePlayers((player) => {
            player.speech = ""
        })

        console.log(`${this.getDetective().name}${this.getDetective().isAI ? ' (AI)' : ''} is the detective !`)
        this.givenTime = ROUND_MAX_TIME
    }

    public setDetectiveQuestion(question: string) {
        const player: Player = this.inGamePlayers.peek();
        player.speech = question;

    }

    public dispatch(action: Action): GameState {
        switch (action.name) {
            case 'joinGame':
                const joiningPlayer = action.value as PlayerDto
                this.lobbyPlayers.enqueue(Player.fromDto(joiningPlayer))
                console.log(`${joiningPlayer.name} enters the lobby`)
                break;
            case 'leaveGame':
                const leavingPlayerId = action.value.playerId
                if (this.isDetective(leavingPlayerId)) {
                    this.inGamePlayers.removePlayer(leavingPlayerId)
                    return new AskingQuestionGameState(this)
                }

                if (this.inGamePlayers.hasPlayer(leavingPlayerId)) {
                    this.inGamePlayers.removePlayer(leavingPlayerId)
                    break
                }

                if (this.lobbyPlayers.hasPlayer(leavingPlayerId)) {
                    this.lobbyPlayers.removePlayer(leavingPlayerId)
                    break
                }

                break;
            case 'speak':
                const speakingPlayerId = action.value.playerId as string
                if (!this.isDetective(speakingPlayerId)) {
                    break
                }
                const speech = action.value.speech as string
                this.setDetectiveQuestion(speech)
                console.log(`detective ${this.getDetective().name} has asked : "${speech}"`)
                return new AnsweringQuestionGameState(this)
            case 'tick':
                if (
                    this.elapsedTimeIsGreaterThan(this.givenTime)
                    ||
                    this.getDetective().speech !== ""
                ) {
                    return new AnsweringQuestionGameState(this)
                }

                // make the detective AI ask a question
                if (
                    this.getDetective().isAI
                    &&
                    this.getDetective().speech === ""
                    &&
                    this.elapsedTimeIsGreaterThan((this.getDetective() as AIPlayer).writingTime)
                ) {
                    (this.getDetective() as AIPlayer).askQuestion(this)
                }
                break;
            default:
                break;
        }
        return this.newGameIfOnlyOnePlayerOrElseCurrentState()
    }
}