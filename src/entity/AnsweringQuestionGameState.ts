import { AccusationsGameState } from './AccusationsGameState.js';
import { AskingQuestionGameState } from './AskingQuestionGameState.js';
import { Action } from '../dto/Action.js';
import { GameState } from "./GameState.js"
import { PlayerDto } from '../dto/PlayerDto.js';
import { Player } from './Player.js';

const ROUND_MAX_TIME = 45
const MIN_AI_ANSWERING_TIME = 10
const MAX_AI_ANSWERING_TIME = 40

export class AnsweringQuestionGameState extends GameState {

    public constructor(previousGameState: GameState | void) {
        if (previousGameState === undefined) {
            super()
            return this
        }
        super(previousGameState)
        if (this.inGamePlayers.size() > 0) {
            // reset all player speeches
            console.log(`${this.inGamePlayers.getOtherPlayers(this.getDetective().id).map(p => p.name).join(', ')} are the Bobs and need to answer !`)
        }
        this.givenTime = ROUND_MAX_TIME
    }

    allPlayersHaveSpoken(): boolean {
        return this.inGamePlayers.toArray().reduce(
            (allPlayersHaveSpoken, player: Player) => {
                return allPlayersHaveSpoken && player.speech !== ""
            },
            true)
    }

    public dispatch(action: Action): GameState {
        switch (action.name) {
            case 'joinGame':
                const joiningPlayer = action.value as PlayerDto
                this.lobbyPlayers.enqueue(Player.fromDto(joiningPlayer))
                console.log(`${joiningPlayer.name} enters the lobby`)
                break;
            case 'leaveGame':
                const leavingPlayerId = action.value.playerId
                if (this.isDetective(leavingPlayerId)) {
                    this.inGamePlayers.removePlayer(leavingPlayerId)
                    return new AnsweringQuestionGameState(this)
                }

                if (this.inGamePlayers.hasPlayer(leavingPlayerId)) {
                    this.inGamePlayers.removePlayer(leavingPlayerId)
                    break
                }

                if (this.lobbyPlayers.hasPlayer(leavingPlayerId)) {
                    this.lobbyPlayers.removePlayer(leavingPlayerId)
                    break
                }

                break;
            case 'speak':
                const speakingPlayerId = action.value.playerId as string
                if (!this.inGamePlayers.hasPlayer(speakingPlayerId)) { break }
                if (this.isDetective(speakingPlayerId)) { break }
                const speakingPlayer = this.inGamePlayers.fetch(speakingPlayerId) as Player
                if (speakingPlayer.speech !== "") { break }
                const speech = action.value.speech as string
                speakingPlayer.speech = speech
                console.log(`${speakingPlayer.name} has answered : "${speech}"`)
                if (this.allPlayersHaveSpoken()) {
                    return new AccusationsGameState(this)
                }
                break
            case 'tick':
                if (this.elapsedTimeIsGreaterThan(this.givenTime)) {
                    return new AccusationsGameState(this)
                }
                
                if (this.allPlayersHaveSpoken()) {
                    return new AccusationsGameState(this)
                }

                // make the bobs AI answer the question
                this.inGamePlayers.getAllAIs().forEach((aiPlayer) => {
                    if (this.getDetective().id === aiPlayer.id) return
                    if (aiPlayer.speech !== "") return
                    if (!this.elapsedTimeIsGreaterThan(aiPlayer.writingTime)) return
                    // console.log(`aiPlayer ${aiPlayer.name} va répondre car sa phrase actuelle est : ${aiPlayer.speech}`, aiPlayer)
                    aiPlayer.answerQuestion(this.getDetective().speech, this)
                })

                break;
            default:
                break;
        }
        return this.newGameIfOnlyOnePlayerOrElseCurrentState()
    }
}