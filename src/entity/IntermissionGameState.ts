import { AskingQuestionGameState } from './AskingQuestionGameState.js';
import { Action } from '../dto/Action.js';
import { GameState } from "./GameState.js"
import { PlayerDto } from '../dto/PlayerDto.js';
import { Player } from './Player.js';
import { AIPlayer } from './AIPlayer.js';

const ROUND_MAX_TIME = 15
const MINIMUM_HUMAN_PLAYERS = 2

export class IntermissionGameState extends GameState {

    public constructor(previousGameState: GameState | void) {
        if (previousGameState === undefined) {
            super()
            return this
        }
        super(previousGameState)

        // high five to the winner !
        this.lastWinner = this.inGamePlayers.getLastHumans()[0]

        // put everybody in the lobby
        this.lobbyPlayers.enqueuePlayersFrom(this.inGamePlayers)

        this.givenTime = ROUND_MAX_TIME
    }

    private renameAllPlayers(){
        const playerNames: string[] = []
        this.inGamePlayers.toArray().forEach(player => {
            playerNames.push(player.giveRandomBobPseudo(playerNames))
        });
    }

    public dispatch(action: Action): GameState {
        switch (action.name) {
            case 'joinGame':
                const joiningPlayer = action.value as PlayerDto
                this.lobbyPlayers.enqueue(Player.fromDto(joiningPlayer))
                console.log(`${joiningPlayer.name} enters the lobby`)
                break;
            case 'leaveGame':
                const leavingPlayerId = action.value.playerId

                if (this.lobbyPlayers.hasPlayer(leavingPlayerId)) {
                    this.lobbyPlayers.removePlayer(leavingPlayerId)
                    break
                }

                break;
            case 'tick':
                if (
                    this.elapsedTimeIsGreaterThan(this.givenTime)
                    &&
                    this.lobbyPlayers.size() >= MINIMUM_HUMAN_PLAYERS
                ) {
                    // le jeu va commencer, on met tout le monde en jeu
                    this.inGamePlayers.enqueuePlayersFrom(this.lobbyPlayers)
                    
                    // on purge les IA qui pourrai rester parce qu'on est parano (ce cas ne devrait pas se produire)
                    this.inGamePlayers.purgeAIs()

                    // on ajoute une IA par joueur
                    const numberOfHumanPlayers = this.inGamePlayers.size()
                    for (let numberOfAIAdded = 0; numberOfAIAdded < numberOfHumanPlayers; numberOfAIAdded++) {
                        this.inGamePlayers.enqueue(new AIPlayer())
                    }

                    // on renomme tous les joueurs
                    this.renameAllPlayers()

                    //on mélange tous les joueurs
                    this.inGamePlayers.shuffle()

                    return new AskingQuestionGameState(this)
                }
                break;
            default:
                break;
        }
        return this
    }
}