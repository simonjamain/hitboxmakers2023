import {PlayerDto} from "../dto/PlayerDto";

export class Player implements PlayerDto {
    id: string = ''
    name: string
    avatar: string = 'bob'
    speech: string = ""
    isAI: boolean = false

    public static fromDto({id, name, speech}: PlayerDto) {
        const player = new Player()
        player.id = id
        player.name = name
        player.speech = speech

        return player
    }

    constructor() {
        this.name = this.generateRandomBobPseudo([])
        this.avatar = this.getRandomAvatar()
    }

    public toDto(): PlayerDto {
        return {
            id: this.id,
            name: this.name,
            speech: this.speech,
            avatar: this.avatar,
        }
    }

    public giveRandomBobPseudo(existingPseudos: string[]) {
        this.name = this.generateRandomBobPseudo(existingPseudos)
        return this.name
    }


    private getRandomAvatar(): string {
        return `bob${this.getRandomInt(0, 13)}`;
    }

    private getRandomInt(min: number, max: number) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    private generateRandomBobPseudo(existingPseudos: string[]): string {
        const options = [
            "Bob le bricoleur",
            "Bob le cuistot",
            "Bob le cascadeur",
            "Bob le musicien",
            "Bob le danseur",
            "Bob le sportif",
            "Bob le rêveur",
            "Bob le voyageur",
            "Bob le jardinier",
            "Bob le magicien",
            "Bob le poète",
            "Bob le clown",
            "Bob le pirate",
            "Bob le joueur",
            "Bob le savant",
            "Bob le paresseux",
            "Bob le farceur",
            "Bob le maladroit",
            "Bob le discret",
            "Bob le mystérieux",
            "Bob le bienveillant",
            "Bob le généreux",
            "Bob le courageux",
            "Bob le téméraire",
            "Bob le loyal",
            "Bob le chevalier",
            "Bob le justicier",
            "Bob le drôle",
            "Bob le farfelu",
            "Bob le bavard",
            "Bob le sérieux",
            "Bob le sage",
            "Bob le curieux",
            "Bob le fouineur",
            "Bob le séducteur",
            "Bob le romantique",
            "Bob le râleur",
            "Bob le grincheux",
            "Bob le grognon",
            "Bob le chanteur",
            "Bob le guitariste",
            "Bob le bassiste",
            "Bob le batteur",
            "Bob le claviériste",
            "Bob le saxophoniste",
            "Bob le trompettiste",
            "Bob le clarinettiste",
            "Bob le flûtiste",
            "Bob l'Ami",
            "Bob l'Artiste",
            "Bob l'Astronaute",
            "Bob l'Audacieux",
            "Bob l'Aventurier",
            "Bob l'Aviateur",
            "Bob l'Ébouriffé",
            "Bob l'Éclectique",
            "Bob l'Électrique",
            "Bob l'Élégant",
            "Bob l'Émerveillé",
            "Bob l'Enchanteur",
            "Bob l'Énergique",
            "Bob l'Enjoué",
            "Bob l'Enthousiaste",
            "Bob l'Époustouflant",
            "Bob l'Équilibré",
            "Bob l'Esquimau",
            "Bob l'Explorateur",
            "Bob l'Extraordinaire",
            "Bob l'Exubérant",
            "Bob l'Imaginatif",
            "Bob l'Incomparable",
            "Bob l'Increvable",
            "Bob l'Indomptable",
            "Bob l'Inoubliable",
            "Bob l'Insaisissable",
            "Bob l'Intemporel",
            "Bob l'Intrépide",
            "Bob l'Inventif",
            "Bob l'Invincible",
            "Bob l'Irrésistible",
        ];

        const filteredOptions = options.filter(option => !existingPseudos.includes(option));

        if (filteredOptions.length === 0) {
            throw new Error("Tous les pseudos possibles sont déjà utilisés");
        }

        const randomIndex = Math.floor(Math.random() * filteredOptions.length);
        const randomPseudo = filteredOptions[randomIndex];

        return randomPseudo;
    }

}