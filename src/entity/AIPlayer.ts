import { Configuration, OpenAIApi } from "openai";
import { Player } from "./Player.js";
import { GameState } from "./GameState";
import { PlayerSpeakDto } from "../dto/PlayerSpeakDto";
import * as dotenv from 'dotenv'
import { v4 as uuidv4 } from 'uuid';

dotenv.config();
dotenv.config({ path: `.env.local`, override: true });

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
})
const openai = new OpenAIApi(configuration)

export class AIPlayer extends Player {
  public isAlreadyFetchingFromOpenAi = false
  public writingTime = 9 + Math.random()*10
  constructor() {
    super()
    this.isAI = true
    this.id = uuidv4()
  }

  public isFetchingLocked():boolean{
    return this.isAlreadyFetchingFromOpenAi
  }

  public lockFetching():void{
    this.isAlreadyFetchingFromOpenAi = true
  }

  public unlockFetching():void{
    this.isAlreadyFetchingFromOpenAi = false
  }

  public answerQuestion(question: string, gameState: GameState) {
    if(this.isFetchingLocked()) return
    this.lockFetching()
    openai.createChatCompletion({
      model: "gpt-3.5-turbo",
      messages: [{
        role: "user",
        content: `Quelle phrase courte dirai bob l'éponge si on lui demandait : "${question}" (donne moi juste la phraseque bob l'éponge dirait)`
      }],
      temperature: 1
    }).then((completion) => {
      const rawResponse = completion.data.choices[0].message?.content ?? '"Je préfère allez chasser la méduse."'
      gameState.dispatch(
        {
          name: 'speak',
          value: {
            speech: rawResponse.substring(1, rawResponse.length-1),
            playerId: this.id
          } as PlayerSpeakDto
        }
      )
    }).catch((error) => {
      gameState.dispatch(
        {
          name: 'speak',
          value: {
            speech: "Je préfère allez jouer avec Sandy.",
            playerId: this.id
          } as PlayerSpeakDto
        }
      )
    }).finally(()=>{
      this.unlockFetching()
    })
  }

  public async askQuestion(gameState: GameState) {
    if(this.isFetchingLocked()) return
    this.lockFetching()
    openai.createChatCompletion({
      model: "gpt-3.5-turbo",
      messages: [{
        role: "user",
        content: `quelle question courte poserais-tu à bob l'éponge si tu souhaitait en apprendre plus sur lui ? (donne moi juste la question directement et ne rajoute pas "bob l'éponge" a la fin de ta phrase)`
      }],
      temperature: 1
    }).then((completion) => {
      const rawResponse = (completion.data.choices[0].message?.content ?? '"Aimes-tu les pâtés de crabes ?"');
      gameState.dispatch(
        {
          name: 'speak',
          value: {
            speech: rawResponse,
            playerId: this.id
          } as PlayerSpeakDto
        }
      )
    }).catch((error) => {
      gameState.dispatch(
        {
          name: 'speak',
          value: {
            speech: "Aimes-tu allez pêcher la méduse ?",
            playerId: this.id
          } as PlayerSpeakDto
        }
      )
    }).finally(()=>{
      this.unlockFetching()
    })
  }
}