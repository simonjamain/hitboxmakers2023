import {PlayerQueue} from '../controlers/PlayerQueue.js';
import {Action} from '../dto/Action.js';
import {GameStateDto} from '../dto/GameStateDto.js';
import {IntermissionGameState} from './IntermissionGameState.js';
import {Player} from './Player.js';

export abstract class GameState {

    public inGamePlayers: PlayerQueue
    public lobbyPlayers: PlayerQueue
    private timeOfCreation_Milliseconds: number
    public lastWinner: Player | undefined
    public givenTime: number = 0

    constructor(
        previousGameState: GameState | void
    ) {
        console.log(`Début de la phase ${this.getName()}`)
        this.timeOfCreation_Milliseconds = Date.now()

        if (previousGameState === undefined) {
            this.inGamePlayers = new PlayerQueue()
            this.lobbyPlayers = new PlayerQueue()
            this.lastWinner = undefined
            return this
        }

        this.inGamePlayers = previousGameState.inGamePlayers
        this.lobbyPlayers = previousGameState.lobbyPlayers
        this.lastWinner = previousGameState.lastWinner
    }

    // public static fromDto(gameStatusDto: GameStatusDto): GameState {
    //     return new GameState(
    //         PlayerQueue.fromArray(gameStatusDto.inGamePlayers.map(Player.fromDto)),
    //         PlayerQueue.fromArray(gameStatusDto.lobbyPlayers.map(Player.fromDto)),
    //     )
    // }

    public toDto(): GameStateDto {
        return {
            inGamePlayers: this.inGamePlayers.toArray(),
            lobbyPlayers: this.lobbyPlayers.toArray(),
            timeOfCreation_Milliseconds: this.timeOfCreation_Milliseconds,
            name: this.getName(),
            lastWinner: this.lastWinner !== undefined ? this.lastWinner.toDto() : undefined,
            countDown: this.givenTime-this.getElapsedTime_Seconds(),
            givenTime: this.givenTime,
        }
    }

    protected newGameIfOnlyOnePlayerOrElseCurrentState(): GameState {
        if (this.inGamePlayers.isOneHumanLeftOrLess()) {
            return new IntermissionGameState(this)
        }
        return this
    }

    public getDetective() {
        return this.inGamePlayers.peek()
    }

    public isDetective(playerId: string) {
        return this.getDetective().id === playerId
    }

    public abstract dispatch(action: Action): GameState

    public getName(): string {
        return this.constructor.name;
    }

    protected resetTimer() {
        this.timeOfCreation_Milliseconds = Date.now()
    }

    protected getElapsedTime_Seconds(): number {
        return (Date.now() - this.timeOfCreation_Milliseconds) / 1000
    }

    protected isItTheTimeForAIToSpeak(elapsedTime: number, minReponseTime: number, maxResponseTime: number) {
        {
            // si < 0 aucune chance de rep
            // si > 0 on répond forcément
            const probabilityToAnswer =
                (elapsedTime - minReponseTime) / maxResponseTime
            console.log(`probabilité de réponse: ${probabilityToAnswer}`)
            return Math.random() < probabilityToAnswer
        }
    }

    protected elapsedTimeIsGreaterThan(seconds: number) {
        return this.getElapsedTime_Seconds() > seconds
    }
}
