import { AskingQuestionGameState } from './AskingQuestionGameState.js';
import { Action } from '../dto/Action.js';
import { GameState } from "./GameState.js"
import { PlayerDto } from '../dto/PlayerDto.js';
import { Player } from './Player.js';
import { PlayerAccusationDto } from '../dto/PlayerAccusationDto.js';

const ROUND_MAX_TIME = 20

export class AccusationsGameState extends GameState {


    public constructor(previousGameState: GameState | void) {
        if (previousGameState === undefined) {
            super()
            return this
        }
        super(previousGameState)
        console.log(`${this.getDetective().name} is the detective !`)
        console.log(`${this.inGamePlayers.getOtherPlayers(this.getDetective().id).map(p => p.name).join(', ')} are the Bobs and need to accuse !`)

        this.givenTime = ROUND_MAX_TIME
    }

    allPlayersHaveSpoken(): boolean {
        return this.inGamePlayers.toArray().reduce(
            (allPlayersHaveSpoken, player: Player) => {
                return allPlayersHaveSpoken && player.speech !== ""
            },
            true)
    }

    public dispatch(action: Action): GameState {
        switch (action.name) {
            case 'joinGame':
                const joiningPlayer = action.value as PlayerDto
                this.lobbyPlayers.enqueue(Player.fromDto(joiningPlayer))
                console.log(`${joiningPlayer.name} enters the lobby`)
                break;
            case 'leaveGame':
                const leavingPlayerId = action.value.playerId
                if (this.isDetective(leavingPlayerId)) {
                    this.inGamePlayers.removePlayer(leavingPlayerId)
                    return new AccusationsGameState(this)
                }

                if (this.inGamePlayers.hasPlayer(leavingPlayerId)) {
                    this.inGamePlayers.removePlayer(leavingPlayerId)
                    break
                }

                if (this.lobbyPlayers.hasPlayer(leavingPlayerId)) {
                    this.lobbyPlayers.removePlayer(leavingPlayerId)
                    break
                }

                break;
            case 'accusePlayer':
                const playerAccusation = action.value as PlayerAccusationDto

                if (!this.inGamePlayers.hasPlayer(playerAccusation.accusedPlayerId)) break
                if (this.isDetective(playerAccusation.accusedPlayerId)) break
                if (playerAccusation.accusedPlayerId === playerAccusation.accusingPlayerId) break

                const accusedPlayer = this.inGamePlayers.fetch(playerAccusation.accusedPlayerId)
                const accusingPlayer = this.inGamePlayers.fetch(playerAccusation.accusingPlayerId)
                if (accusedPlayer.isAI) {
                    console.log(`${accusingPlayer.name} à accusé ${accusedPlayer.name} à tord, oups !`)
                    this.lobbyPlayers.enqueue(this.inGamePlayers.extractPlayer(accusingPlayer.id))
                }
                if (!accusedPlayer.isAI) {
                    console.log(`${accusingPlayer.name} à accusé ${accusedPlayer.name} et il a raison, bien joué !`)
                    this.lobbyPlayers.enqueue(this.inGamePlayers.extractPlayer(accusedPlayer.id))
                }

            case 'tick':
                if (this.elapsedTimeIsGreaterThan(this.givenTime)) {
                    // on change le prochain détective
                    this.inGamePlayers.goToNextPlayer()
                    return new AskingQuestionGameState(this)
                }
                break;
            default:
                break;
        }
        return this.newGameIfOnlyOnePlayerOrElseCurrentState()
    }
}