import { PlayerDto } from "./PlayerDto";

export interface GameStateDto {
    inGamePlayers: PlayerDto[]
    lobbyPlayers: PlayerDto[]
    timeOfCreation_Milliseconds: number
    name: string
    countDown: number
    givenTime: number
    lastWinner: PlayerDto | undefined
}