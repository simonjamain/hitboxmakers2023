export interface PlayerDto {
    id: string,
    name: string,
    avatar: string,
    speech: string,
}