export interface PlayerSpeakDto{
    speech: string
    playerId: string
}