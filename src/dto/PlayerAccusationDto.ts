export interface PlayerAccusationDto{
    accusedPlayerId: string
    accusingPlayerId: string
}