import geckos from '@geckos.io/client'
import {FrameRate} from './controlers/FrameRate'
import { PlayerDto } from './dto/PlayerDto'
import { Player } from './entity/Player';

import { createApp } from 'vue'
import App from './App.vue'

const app = createApp(App).mount('#app')
